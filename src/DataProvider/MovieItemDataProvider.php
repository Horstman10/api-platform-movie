<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Movie;
use App\Service\MovieService;
use GuzzleHttp\Exception\GuzzleException;

final class MovieItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var MovieService
     */
    private MovieService $movieService;

    /**
     * MovieCollectionDataProvider constructor.
     * @param MovieService $movieService
     */
    public function __construct(
        MovieService $movieService
    ) {
        $this->movieService = $movieService;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Movie::class === $resourceClass;
    }

    /**
     * @param string $resourceClass
     * @param array|int|string $id
     * @param string|null $operationName
     * @param array $context
     * @return Movie|null
     * @throws GuzzleException
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Movie
    {
        return $this->movieService->getMovie($id);
    }
}
