<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Movie;
use App\Exception\Movie\MovieListNotFound;
use App\Exception\Technical\TechnicalErrorException;
use App\Service\MovieService;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\RequestStack;

final class MovieCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var MovieService
     */
    private MovieService $movieService;
    /**
     * @var RequestStack
     */
    private RequestStack $requestStack;

    /**
     * MovieCollectionDataProvider constructor.
     * @param MovieService $movieService
     * @param RequestStack $requestStack
     */
    public function __construct(
        MovieService $movieService,
        RequestStack $requestStack
    ) {
        $this->requestStack = $requestStack;
        $this->movieService = $movieService;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Movie::class === $resourceClass;
    }

    /**
     * @param string $resourceClass
     * @param string|null $operationName
     * @return array
     * @throws GuzzleException
     * @throws MovieListNotFound
     * @throws TechnicalErrorException
     */
    public function getCollection(string $resourceClass, string $operationName = null): array
    {
        $request = $this->requestStack->getCurrentRequest();

        $index = explode('\\', $resourceClass);
        $index = strtolower(end($index));

        $filters = $request->query->all();
        return $this->movieService->searchMovie(
            ($filters !== null && key_exists('search', $filters))? $filters['search']: 'a',
            ($filters !== null && key_exists('includeAdult', $filters))? boolval($filters['includeAdult']): 'a',
            ($filters !== null && key_exists('year', $filters))? intval($filters['year']): 'a',
            ($filters !== null && key_exists('page', $filters))? intval($filters['page']): 'a'

        );
    }
}
