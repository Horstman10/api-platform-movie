<?php

namespace App\Exception\Parameter;

use App\Entity\Error\ErrorType;
use App\Exception\ApiException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ConstraintViolationParameterException
 * @package App\Exception\Parameter
 */
class ConstraintViolationParameterException extends ApiException
{
    protected $message = ErrorType::CONSTRAINT_VIOLATION_PARAMETER;
    protected $httpCode = JsonResponse::HTTP_BAD_REQUEST;

    public function __construct(string $fieldName = '', string $violation = '')
    {
        parent::__construct($this->message, $this->httpCode, [$fieldName, $violation]);
    }
}
