<?php

namespace App\Exception;

use App\Entity\Error\ErrorType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ApiIllegalAccessException
 * @package App\Exception
 */
class ApiIllegalAccessException extends ApiException
{
    protected $message = ErrorType::API_ACCESS_FORBIDDEN;
    protected $httpCode = JsonResponse::HTTP_FORBIDDEN;

    /**
     * ApiIllegalAccessException constructor.
     */
    public function __construct()
    {
        parent::__construct($this->message, $this->httpCode);
    }
}
