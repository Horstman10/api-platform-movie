<?php

namespace App\Exception;

use App\Entity\Error\ErrorType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class InternalErrorException
 * @package App\Exception
 */
class InternalErrorException extends ApiException
{
    /**
     * @var string $message
     */
    protected $message = ErrorType::INTERNAL_ERROR;

    /**
     * @var int $httpCode
     */
    protected $httpCode = Response::HTTP_INTERNAL_SERVER_ERROR;

    /**
     * InternalErrorException constructor.
     * @throws Technical\MissingCodeException
     */
    public function __construct()
    {
        parent::__construct(
            $this->message,
            $this->httpCode,
            []
        );
    }
}
