<?php

namespace App\Exception\Movie;

use App\Entity\Error\ErrorType;
use App\Exception\ApiException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class MovieListNotFound
 * @package App\Exception\Movie
 */
class MovieListNotFound extends ApiException
{
    protected $message = ErrorType::MOVIE_LIST_NOT_FOUND_ID;
    protected $httpCode = JsonResponse::HTTP_NOT_FOUND;

    public function __construct()
    {
        parent::__construct($this->message, $this->httpCode, []);
    }
}
